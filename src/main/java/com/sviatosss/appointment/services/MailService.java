package com.sviatosss.appointment.services;

import com.sviatosss.appointment.entity.timetable.Appointment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

@Service
public class MailService {

    @Autowired
    private MailContentBuilder mailContentBuilder;

    @Autowired
    private UserService userService;

    private JavaMailSender mailSender;

    @Autowired
    public MailService(JavaMailSender mailSender) {
        this.mailSender = mailSender;
    }

    void sendingEmailsNewAppointment(Appointment appointment){
        // to student
        String recipient = userService.getCurrentUser().getEmail();
        prepareAndSend(recipient, appointment.getApprove(), appointment.getDenied(), false);

        // to teacher
        recipient = userService.get(appointment.getTeacher()).getEmail();
        prepareAndSend(recipient, appointment.getApprove(), appointment.getDenied(), true);
    }

    void sendingEmailWelcome(String email){
        String url = "https://appointmentsss.herokuapp.com/swagger-ui.html";
        prepareAndSend(email, url);
    }


    private void prepareAndSend(String recipient, String url) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom("appointment.appss@gmail.com");
            messageHelper.setTo(recipient);
            messageHelper.setSubject("Welcome to app !");
            String content = mailContentBuilder.build(url);
            messageHelper.setText(content, true);
        };
        try {
            mailSender.send(messagePreparator);
        } catch (MailException e) {
            // runtime exception; compiler will not force you to handle it
        }
    }

    private void prepareAndSend(String recipient, String approve, String denied, Boolean teacher) {
        MimeMessagePreparator messagePerpetrator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom("appointment.appss@gmail.com");
            messageHelper.setTo(recipient);
            messageHelper.setSubject("NEW appointment !");
            String content = mailContentBuilder.build(approve, denied, teacher);
            messageHelper.setText(content, true);
        };
        try {
            mailSender.send(messagePerpetrator);
        } catch (MailException e) {
            // runtime exception; compiler will not force you to handle it
        }
    }

}
