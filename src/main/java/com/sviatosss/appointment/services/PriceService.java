package com.sviatosss.appointment.services;

import com.sviatosss.appointment.entity.Price;
import com.sviatosss.appointment.entity.Teacher;
import com.sviatosss.appointment.repository.PriceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class PriceService {
    @Autowired
    private TeacherService teacherService;

    @Autowired
    private PriceRepository priceRepository;

    public Price addPrice(Price price) {
        Teacher teacher = teacherService.getCurrentTeacher();
        price.setTeacher(teacher);
        Boolean exist = priceRepository.existsByMinutesAndAndTeacher(price.getMinutes(), price.getTeacher());
        if (exist){
            BigDecimal newPrive = price.getPrice();
            price = priceRepository.findByMinutesAndAndTeacher(price.getMinutes(), price.getTeacher()).get();
            price.setPrice(newPrive);
        }
        return priceRepository.save(price);
    }

    public BigDecimal getPrice(Long minutes, Teacher teacher){
        Boolean exist = priceRepository.existsByMinutesAndAndTeacher(minutes, teacher);
        if (exist){
            Price price = priceRepository.findByMinutesAndAndTeacher(minutes, teacher).get();
            return price.getPrice();
        }
        return null;
    }
}
