package com.sviatosss.appointment.services;

import com.sviatosss.appointment.entity.Teacher;
import com.sviatosss.appointment.entity.timetable.FreeTime;
import com.sviatosss.appointment.exceptions.IDException;
import com.sviatosss.appointment.repository.TeacherRepository;
import com.sviatosss.appointment.utils.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TeacherService {
    @Autowired
    private TeacherRepository teacherRepository;

    @Autowired
    private UserService userService;

    private Validator validator = new Validator();

    public Teacher getCurrentTeacher(){
        return teacherRepository.findById(userService.getCurrentUser().getId())
                .orElseThrow(() -> new IDException("Couldn't get teacher. Teacher - " + " doesn't exist"));
    }

    Teacher getById(Long id){
        return teacherRepository.findById(id)
                .orElseThrow(() -> new IDException("Couldn't get teacher. Teacher - " + id + " doesn't exist"));
    }

    void save(Teacher teacher) {
        teacherRepository.save(teacher);
    }

    public boolean isIntersect(FreeTime freeTime){
        Teacher teacher = getCurrentTeacher();
        List<FreeTime> freeTimeList = teacher.getFreeTimeList();
        for (FreeTime f : freeTimeList){
            if (validator.isIntersect(freeTime, f)) return true;
            if (validator.ifEqual(freeTime, f)) return true;
        }
        return false;
    }
}
