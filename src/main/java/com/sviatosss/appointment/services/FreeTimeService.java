package com.sviatosss.appointment.services;

import com.sviatosss.appointment.entity.timetable.FreeTime;
import com.sviatosss.appointment.repository.FreeTimeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FreeTimeService {
    @Autowired
    private TeacherService teacherService;

    @Autowired
    private FreeTimeRepository freeTimeRepository;

    public FreeTime addFreeTime(FreeTime time) {
        time.setTeacher(teacherService.getCurrentTeacher());
        return freeTimeRepository.save(time);
    }

    FreeTime save(FreeTime time) {
        return freeTimeRepository.save(time);
    }
}
