package com.sviatosss.appointment.services;

import com.sviatosss.appointment.entity.Enums.EStatus;
import com.sviatosss.appointment.entity.Teacher;
import com.sviatosss.appointment.entity.timetable.Appointment;
import com.sviatosss.appointment.entity.timetable.FreeTime;
import com.sviatosss.appointment.exceptions.IDException;
import com.sviatosss.appointment.repository.AppointmentRepository;
import com.sviatosss.appointment.utils.Generator;
import com.sviatosss.appointment.utils.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AppointmentService {
    @Autowired
    private FreeTimeService freeTimeService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private UserService userService;

    @Autowired
    private AppointmentRepository appointmentRepository;

    @Autowired
    private MailService mailService;

    @Autowired
    private PriceService priceService;

    private Validator validator = new Validator();

    public Appointment createAppointment(Appointment appointment) {
        appointment.setStatus(EStatus.NOT_APPROVE);
        linksSet(appointment);
        appointment.setStudent(userService.getCurrentUser().getId());
        appointment.setPrice(priceService.getPrice(new Generator().minutes(appointment.getStarting(), appointment.getEnding()),
                teacherService.getById(appointment.getTeacher())));
        appointment =  appointmentRepository.save(appointment);
        mailService.sendingEmailsNewAppointment(appointment);
        return appointment;
    }

    public boolean checkIfFree(Appointment appointment){
        Teacher teacher = teacherService.getById(appointment.getTeacher());
        List<FreeTime> freeTimeList = teacher.getFreeTimeList();
        for (FreeTime f : freeTimeList){
            // cutting out... appointment time from free time
            if (isInTheMiddle(appointment, f)) return true;
            if (isInTheBeginning(appointment, f)) return true;
            if (isInTheEnd(appointment, f)) return true;
            if (ifEqual(appointment, f, teacher, freeTimeList)) return true;
        }
        return false;
    }

    private boolean isInTheMiddle(Appointment appointment, FreeTime f){
        if (validator.isInTheMiddle(appointment, f)){
            FreeTime freeTime = new FreeTime(f.getTeacher());
            freeTime.setEnding(f.getEnding());
            freeTime.setStarting(appointment.getEnding());
            f.setEnding(appointment.getStarting());
            freeTimeService.save(f);
            freeTimeService.save(freeTime);
            return true;
        }
        return false;
    }

    private boolean isInTheBeginning(Appointment appointment, FreeTime f){
        if (validator.isInTheBeginning(appointment, f)){
            f.setStarting(appointment.getEnding());
            freeTimeService.save(f);
            return true;
        }
        return false;
    }

    private boolean isInTheEnd(Appointment appointment, FreeTime f) {
        if (validator.isInTheEnd(appointment, f)){
            f.setEnding(appointment.getStarting());
            freeTimeService.save(f);
            return true;
        }
        return false;
    }

    private boolean ifEqual(Appointment appointment, FreeTime f, Teacher teacher, List<FreeTime> freeTimeList) {
        if (validator.ifEqual(appointment, f)){
            System.out.println(f.getId());
            freeTimeList.remove(f);
            teacher.setFreeTimeList(freeTimeList);
            teacherService.save(teacher);
            return true;
        }
        return false;
    }

    public Appointment approveAppointment(String id){
        if (id.matches("\\d+")) {
            Appointment appointment = appointmentRepository.findById(Long.parseLong(id))
                    .orElseThrow(() -> new IDException("Couldn't get appointment. ID - " + id + " doesn't exist"));
            if (userService.getCurrentUser().getId().equals(appointment.getTeacher())) {
                appointment.setStatus(EStatus.APPROVE);
            }
            return save(appointment);
        }else {
            Appointment appointment = appointmentRepository.findByApprove((String) id)
                    .orElseThrow(() -> new IDException("Couldn't get appointment. Invalid link - " + id));
            appointment.setStatus(EStatus.APPROVE);
            return save(appointment);
        }
    }

    public Appointment deniedAppointment(String id){
        if (id.matches("\\d+")){
            Appointment appointment = appointmentRepository.findById(Long.parseLong(id))
                    .orElseThrow(() -> new IDException("Couldn't get appointment. ID - " + id + " doesn't exist"));
            if (userService.getCurrentUser().getId().equals(appointment.getTeacher())
                    || userService.getCurrentUser().getId().equals(appointment.getStudent()) ) {
                appointment.setStatus(EStatus.DENIED);
            }
            return save(appointment);
        }else {
            Appointment appointment = appointmentRepository.findByDenied(id)
                    .orElseThrow(() -> new IDException("Couldn't get appointment. Invalid link - " + id));
            appointment.setStatus(EStatus.DENIED);
            return save(appointment);
        }
    }

    private Appointment save(Appointment appointment){
        return appointmentRepository.save(appointment);
    }

    private void linksSet(Appointment appointment){
        String link = "";

        do {
            link = new Generator().codeGeneration();
        }while (appointmentRepository.existsByApprove(link));
        appointment.setApprove(link);

        do {
            link = new Generator().codeGeneration();
        }while (appointmentRepository.existsByDenied(link));
        appointment.setDenied(link);
    }
}