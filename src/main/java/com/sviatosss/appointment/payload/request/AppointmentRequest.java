package com.sviatosss.appointment.payload.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Getter
@Setter
public class AppointmentRequest {
    @NotNull
    private Long teacher;

    @NotNull
    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
    private Date starting;

    @NotNull
    @JsonFormat(pattern="yyyy-MM-dd HH:mm")
    private Date ending;
}
