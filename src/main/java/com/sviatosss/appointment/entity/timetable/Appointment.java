package com.sviatosss.appointment.entity.timetable;

import com.sviatosss.appointment.entity.Enums.EStatus;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.Date;

@Entity
@Getter
@Setter
@Table(name = "appointments")
public class Appointment extends BaseTimeTable {
    private String approve;
    private String denied;

    private BigDecimal price;

    private Long teacher;
    private Long student;

    public Appointment(Long teacher, Date start, Date end){
        this.teacher = teacher;
        setStarting(start);
        setEnding(end);
    }

    public Appointment(){}

    @Enumerated(EnumType.STRING)
    private EStatus status;
}
