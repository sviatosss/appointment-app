package com.sviatosss.appointment.entity.timetable;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.sviatosss.appointment.entity.Teacher;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "free_time")
public class FreeTime extends BaseTimeTable {
    @JsonBackReference
    @ManyToOne
    @JoinColumn(name = "teacher_id", nullable = false)
    private Teacher teacher;
}
