package com.sviatosss.appointment.entity.Enums;

public enum EStatus {
    NOT_APPROVE,
    APPROVE,
    DENIED,
}