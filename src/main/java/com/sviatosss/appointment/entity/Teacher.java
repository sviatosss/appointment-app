package com.sviatosss.appointment.entity;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.sviatosss.appointment.entity.timetable.FreeTime;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "teachers")
public class Teacher {
    @Id
    private Long id;

    public Teacher(Long id){
        this.id = id;
    }

    @JsonManagedReference
    @OneToMany(mappedBy = "teacher", orphanRemoval = true, cascade = CascadeType.PERSIST)
    private List<Price> priceList = new ArrayList<>();

    @JsonManagedReference
    @OneToMany(mappedBy = "teacher", orphanRemoval = true, cascade = CascadeType.PERSIST)
    private List<FreeTime> freeTimeList = new ArrayList<>();
}
