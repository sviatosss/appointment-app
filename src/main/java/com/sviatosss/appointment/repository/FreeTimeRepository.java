package com.sviatosss.appointment.repository;

import com.sviatosss.appointment.entity.timetable.FreeTime;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FreeTimeRepository extends JpaRepository<FreeTime, Long> {

}