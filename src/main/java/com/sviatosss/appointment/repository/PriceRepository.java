package com.sviatosss.appointment.repository;

import com.sviatosss.appointment.entity.Price;
import com.sviatosss.appointment.entity.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.Optional;

@Repository
public interface PriceRepository extends JpaRepository<Price, Long> {

    Optional<Price> findByMinutesAndAndTeacher(Long minutes, Teacher teacher);

    Boolean existsByMinutesAndAndTeacher(Long minutes, Teacher teacher);
}
