package com.sviatosss.appointment.repository;

import com.sviatosss.appointment.entity.timetable.Appointment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AppointmentRepository extends JpaRepository<Appointment, Long> {
    Optional<Appointment> findByApprove(String approve);
    Optional<Appointment> findByDenied(String denied);

    Boolean existsByApprove(String approve);
    Boolean existsByDenied(String denied);
}