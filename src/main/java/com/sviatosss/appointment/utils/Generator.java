package com.sviatosss.appointment.utils;

import java.util.Date;
import java.util.Random;

public class Generator {
    public String codeGeneration(){
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = 60;
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    public Long minutes(Date start, Date end){
        long diff = end.getTime() - start.getTime();
        return diff / (60 * 1000);
    }
}
