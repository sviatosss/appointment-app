package com.sviatosss.appointment.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sviatosss.appointment.entity.timetable.BaseTimeTable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.Date;
import java.util.List;

public class Validator {
    public boolean checkDate(Date start, Date end){
        if(start.compareTo(end) > 0) {
            System.out.println("Date 1 occurs after Date 2");
            return true;
        }return false;
    }

    public ResponseEntity<?> getErrors(BindingResult bindingResult) throws JsonProcessingException {
        System.out.println(bindingResult.toString());
        StringBuilder messages = new StringBuilder("{\n");
        List<FieldError> errors = bindingResult.getFieldErrors();
        for (FieldError error : errors ) {
            messages.append("\"").append(error.getField()).append("\" : \"").append(error.getDefaultMessage()).append("\",\n");
        }
        messages.deleteCharAt(messages.length()-2).append("}");
        ObjectMapper mapper = new ObjectMapper();
        JsonNode jsonObject = mapper.readTree(String.valueOf(messages));

        return new ResponseEntity<>(jsonObject, HttpStatus.BAD_REQUEST);
    }

    public boolean isIntersect(BaseTimeTable appointment, BaseTimeTable f) {
        return (appointment.getStarting().after(f.getStarting()) && appointment.getStarting().before(f.getEnding()))
                || (appointment.getEnding().after(f.getStarting()) && appointment.getEnding().before(f.getEnding()));
    }
    public boolean isInTheMiddle(BaseTimeTable appointment, BaseTimeTable f){
        return (appointment.getStarting().after(f.getStarting()) && appointment.getStarting().before(f.getEnding()))
                && (appointment.getEnding().after(f.getStarting()) && appointment.getEnding().before(f.getEnding()));
    }
    public boolean isInTheBeginning(BaseTimeTable appointment, BaseTimeTable f){
        return (appointment.getStarting().equals(f.getStarting())
                && (appointment.getEnding().after(f.getStarting()) && appointment.getEnding().before(f.getEnding())));
    }
    public boolean isInTheEnd(BaseTimeTable appointment, BaseTimeTable f){
        return (appointment.getEnding().equals(f.getEnding())
                && (appointment.getStarting().after(f.getStarting()) && appointment.getStarting().before(f.getEnding())));
    }
    public boolean ifEqual(BaseTimeTable appointment, BaseTimeTable f){
        return ((appointment.getEnding().equals(f.getEnding())) && (appointment.getStarting().equals(f.getStarting())));
    }
}
