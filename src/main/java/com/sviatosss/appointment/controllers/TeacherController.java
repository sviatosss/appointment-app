package com.sviatosss.appointment.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sviatosss.appointment.entity.Price;
import com.sviatosss.appointment.entity.timetable.Appointment;
import com.sviatosss.appointment.entity.timetable.FreeTime;
import com.sviatosss.appointment.services.AppointmentService;
import com.sviatosss.appointment.services.FreeTimeService;
import com.sviatosss.appointment.services.PriceService;
import com.sviatosss.appointment.services.TeacherService;
import com.sviatosss.appointment.utils.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/teacher")
public class TeacherController {
    @Autowired
    private FreeTimeService freeTimeService;

    @Autowired
    private TeacherService teacherService;

    @Autowired
    private PriceService priceService;

    @Autowired
    private AppointmentService appointmentService;


    @PostMapping("/add-price")
    public ResponseEntity<?> createPrice(@Valid @RequestBody Price price, BindingResult bindingResult) throws JsonProcessingException {
        if (bindingResult.hasErrors()){
            return new Validator().getErrors(bindingResult);
        }
        return new ResponseEntity<>(priceService.addPrice(price), HttpStatus.OK);
    }

    @PostMapping("/add-free-time")
    public ResponseEntity<?> addFreeTime(@Valid @RequestBody FreeTime time, BindingResult bindingResult) throws JsonProcessingException {
        if (bindingResult.hasErrors()){
            return new Validator().getErrors(bindingResult);
        }
        if (new Validator().checkDate(time.getStarting(), time.getEnding())){
            return new ResponseEntity<>("ERROR !!! INCORRECT DATA !", HttpStatus.BAD_REQUEST);
        }
        if (teacherService.isIntersect(time)){
            return new ResponseEntity<>("ERROR !!! DATE DOESN'T ALLOW !", HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>(freeTimeService.addFreeTime(time), HttpStatus.OK);
    }

    @GetMapping("/appointment/{id}/approve")
    public Appointment approveAppointment(@PathVariable String id) {
        return appointmentService.approveAppointment(id);
    }

    @GetMapping("/appointment/{id}/denied")
    public Appointment deniedAppointment(@PathVariable String id) {
        return appointmentService.deniedAppointment(id);
    }

}
