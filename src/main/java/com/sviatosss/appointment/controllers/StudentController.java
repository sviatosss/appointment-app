package com.sviatosss.appointment.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.sviatosss.appointment.entity.timetable.Appointment;
import com.sviatosss.appointment.payload.request.AppointmentRequest;
import com.sviatosss.appointment.services.AppointmentService;
import com.sviatosss.appointment.utils.Validator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/student")
public class StudentController {
    @Autowired
    private AppointmentService appointmentService;

    @PostMapping("/add-appointment")
    public ResponseEntity<?> createAppointment(@Valid @RequestBody AppointmentRequest request, BindingResult bindingResult) throws JsonProcessingException {
        if (bindingResult.hasErrors()){
            return new Validator().getErrors(bindingResult);
        }
        if (new Validator().checkDate(request.getStarting(), request.getEnding())){
            return new ResponseEntity<>("ERROR !!! INCORRECT DATA !", HttpStatus.BAD_REQUEST);
        }
        Appointment appointment = new Appointment(request.getTeacher(), request.getStarting(), request.getEnding());
        if (!appointmentService.checkIfFree(appointment)){
            return new ResponseEntity<>("ERROR !!! DATE DOESN'T ALLOW !", HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(appointmentService.createAppointment(appointment), HttpStatus.OK);
    }

    @GetMapping("/appointment/{id}/denied")
    public Appointment deniedAppointment(@Valid @PathVariable String id) {
        return appointmentService.deniedAppointment(id);
    }
}
