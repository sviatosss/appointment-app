package com.sviatosss.appointment.controllers;

import com.sviatosss.appointment.entity.Enums.EStatus;
import com.sviatosss.appointment.services.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/v1/link")
public class LinkController {
    @Autowired
    private AppointmentService appointmentService;

    @GetMapping("/appointment/{id}/denied")
    public EStatus deniedAppointment(@Valid @PathVariable String id) {
        return appointmentService.deniedAppointment(id).getStatus();
    }

    @GetMapping("/appointment/{id}/approve")
    public EStatus approveAppointment(@Valid @PathVariable String id) {
        return appointmentService.approveAppointment(id).getStatus();
    }
}
